const express = require('express')
const bodyParser = require('body-parser')
const path = require('path')
const { MongoClient } = require('mongodb')
const { handleOrderTasks } = require('./orderHandler')

const dbUrl = 'mongodb://localhost:27017/carenderia'
let db
const app = express()
const port = process.env.PORT || 8000

async function startServer () {
	app
		.use(bodyParser.urlencoded({ extended: true }))
		.use(bodyParser.json())
		.use(express.static(path.join(process.cwd(), 'public')))
	try {
		const client = await MongoClient.connect(dbUrl)
		db = await client.db('carenderia')
		await app.listen(port)
		console.log(`App @ http://localhost:${port}`)
		handleOrderTasks(app, db)
	} catch (error) {
		console.log('Start Server Error', error)
	}
}

startServer()
