import React from 'react'
import { Button, ListGroupItem, Panel, Glyphicon } from 'react-bootstrap'

const panel = {
	backgroundColor: '#f5fff5',
	fontWeight: 'bold'
}

const glyphicon = {
	backgroundColor: '#006622',
	color: 'white'
}

const button = {
	textAlign: 'right'
}

class OrderCard extends React.Component {
	constructor (props) {
		super(props)
	}

	render () {
		const buttonDisabled = this.props.order.status === 'pending'
		return (
			<ListGroupItem>
				<Panel>
					<Panel.Heading style={panel}>Table {`${this.props.order.tableNumber}`}</Panel.Heading>
					<Panel.Body>
						<ul>
							{this.props.order.meals.map((meal) => {
								return <li key={meal}>{meal}</li>
							})}
						</ul>
					</Panel.Body>
					<Panel.Footer style={panel}>
						<div style={button}>
							<Button
								bsSize="small"
								style={glyphicon}
								disabled={buttonDisabled}
								onClick={this.props.updateFunction.bind(this, this.props.order, 'prev')}
							>
								<Glyphicon glyph="chevron-left" />
							</Button>
							<Button
								bsSize="small"
								style={glyphicon}
								onClick={this.props.updateFunction.bind(this, this.props.order, 'next')}
							>
								<Glyphicon glyph="chevron-right" />
							</Button>
						</div>
					</Panel.Footer>
				</Panel>
			</ListGroupItem>
		)
	}
}

export default OrderCard
