import React from 'react'
import { Panel, ListGroup, Badge } from 'react-bootstrap'
import OrderCard from './OrderCard'

const heading = {
	fontWeight: 'bold',
	backgroundColor: '#00b33c',
	color: 'white'
}

const badge = {
	marginLeft: '5px',
	backgroundColor: '#006622'
}

class OrderTab extends React.Component {
	constructor(props) {
		super(props)
	}

	render() {
		return (
			<Panel>
				<Panel.Heading style={heading}>
					{this.props.name}
					<Badge style={badge}>{this.props.orders.length}</Badge>
				</Panel.Heading>
				<ListGroup>
					{this.props.orders.map((order) => {
						return <OrderCard key={order._id} order={order} updateFunction={this.props.updateFunction} />
					})}
				</ListGroup>
			</Panel>
		)
	}
}

export default OrderTab
