import React from 'react'
import {Navbar} from 'react-bootstrap'

const header = {
  backgroundColor: '#00cc66',
  fontWeight: 'bold'
}

const navbarBrand = {
  color: '#ffffff',
  fontSize: '20px'
}

class Header extends React.Component {
  render() {
    return (
      <Navbar style={header}>
        <Navbar.Header>
          <Navbar.Brand style={navbarBrand} >
            Key Itchy Hen
          </Navbar.Brand>
        </Navbar.Header>
      </Navbar>
    )
  }
}

export default Header