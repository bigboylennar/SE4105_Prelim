import React from 'react'
import { Grid, Col, Row } from 'react-bootstrap'
import OrderTab from './OrderTab'

class Body extends React.Component {
	constructor (props) {
		super(props)

		this.state = {
			pending: [],
			preparing: [],
			ready: []
		}
		this.orders = new Map()
		this.statuses = [ 'pending', 'preparing', 'ready' ]
		this.updateObj = {
			minDelay: 1,
			maxDelay: 55,
			minPrevDelay: 1,
			delay: 0,
			prevDelay: 0,
			lastUpdate: ''
		}
	}

	async componentDidMount () {
		this.update()
	}

	async fetchOrders () {
		const result = await fetch(`/orders/${this.updateObj.lastUpdate}`)
		const orders = await result.json()
		return orders
	}

	async updateOrders () {
		const newOrders = await this.fetchOrders()
		newOrders.map((order) => {
			this.orders.set(`${order._id}`, order)
		})

		if (newOrders.length !== 0) {
			this.updateObj.delay = this.updateObj.minDelay
			this.updateObj.prevDelay = this.updateObj.minPrevDelay
			this.filterOrders(this.orders)
		} else if (this.updateObj.delay < this.updateObj.maxDelay) {
			const prev = this.updateObj.delay
			this.updateObj.delay += this.updateObj.prevDelay
			this.updateObj.prevDelay = prev
		}
		this.updateObj.lastUpdate = new Date().toISOString()
		console.log(this.updateObj.delay)
	}

	async updateOrder (order, action) {
		const statusUpdates = {
			pending: {
				next: 'preparing'
			},
			preparing: {
				prev: 'pending',
				next: 'ready'
			},
			ready: {
				prev: 'preparing',
				next: 'delivered'
			}
		}
		const data = {
			status: statusUpdates[order.status][action],
			lastUpdate: new Date()
		}
		const res = await fetch(`/orders/update/${order._id}`, {
			method: 'POST',
			body: JSON.stringify(data),
			headers: new Headers({
				'Content-Type': 'application/json'
			})
		})
		if (res.status === 200) {
			this.removeOrderFromArray(this.state[order.status], order)
			order.status = statusUpdates[order.status][action]
			if (order.status !== 'delivered') {
				this.addOrderToArray(order)
			} else {
				this.forceUpdate()
			}
		}
	}

	addOrderToArray (order) {
		const orderArrays = {
			pending: [],
			preparing: [],
			ready: []
		}
		orderArrays[order.status].push(order)
		this.setState({
			pending: this.state.pending.concat(orderArrays.pending),
			preparing: this.state.preparing.concat(orderArrays.preparing),
			ready: this.state.ready.concat(orderArrays.ready)
		})
	}

	removeOrderFromArray (array, order) {
		const index = array.indexOf(order)

		if (index !== -1) {
			array.splice(index, 1)
		}
	}

	filterOrders (orders) {
		const filteredOrders = {
			pending: [],
			preparing: [],
			ready: []
		}
		orders.forEach((order, id, map) => {
			if (order.status === 'pending') {
				filteredOrders.pending.push(order)
			} else if (order.status === 'preparing') {
				filteredOrders.preparing.push(order)
			} else if (order.status === 'ready') {
				filteredOrders.ready.push(order)
			}
		})

		this.setState({
			pending: filteredOrders.pending,
			preparing: filteredOrders.preparing,
			ready: filteredOrders.ready
		})
	}

	update () {
		setTimeout(() => {
			this.updateOrders()
			this.update()
		}, this.updateObj.delay * 1000)
	}

	render () {
		return (
			<Grid className="container-fluid">
				<Row>
					{this.statuses.map((status) => {
						return (
							<Col sm={4} key={status}>
								<OrderTab
									name={status}
									orders={this.state[status]}
									updateFunction={this.updateOrder.bind(this)}
								/>
							</Col>
						)
					})}
				</Row>
			</Grid>
		)
	}
}

export default Body
