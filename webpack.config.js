const path = require('path')

module.exports = {
  entry: './public/index.jsx',
  output: {
    filename: 'bundle.js',
    path: path.join(process.cwd(), 'public')
  },
  module: {
    loaders: [{
      test: /\.jsx?$/,
      exclude: /node_modules/,
      loader: 'babel-loader'
    }]
  },
  resolve : {
    extensions: ['.js', '.jsx']
  }
}
