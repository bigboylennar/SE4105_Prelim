const { ObjectId } = require('mongodb')

const handleOrderTasks = (app, db) => {
	const getAllOrders = async (request, response) => {
		try {
			const orders = await db.collection('orders').find().toArray()
			response.json(orders)
		} catch (error) {
			response.status(404)
			console.log(error)
		}
  }
  
  const getNewOrders = async (request, response) => {
		try {
			const { updated } = request.params
			const orders = await db
				.collection('orders')
				.find({ timeUpdated: { $gt: new Date(`${updated}`) } })
				.toArray()
			response.json(orders)
		} catch (error) {
			response.status(404)
			console.log(error)
		}
	}

	const updateStatus = async (request, response) => {
		try {
			const { status, lastUpdate } = request.body
			const { id } = request.params
			const res = await db
				.collection('orders')
				.update({ _id: ObjectId(id) }, { $set: { status: status, timeUpdated: new Date(lastUpdate) } })
			response.json(res)
		} catch (error) {
			response.status(500)
			console.log(error)
		}
	}

  app.post('/orders/update/:id', updateStatus)
	app.get('/orders/:updated', getNewOrders)
	app.get('/orders', getAllOrders)
}

module.exports.handleOrderTasks = handleOrderTasks
